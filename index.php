<?php
require __DIR__ . '/vendor/autoload.php';
use GuzzleHttp\Psr7\ServerRequest;
use Sorani\RouterGrafik\Router;
use Sorani\RouterGrafik\RouterContainer;
use Sorani\RouterGrafik\RouterInstanciator;

$request = ServerRequest::fromGlobals();

// $router = new RouterInstanciator();
$router = new Router();

$router->map('/pages/:id-:slug', [App\PageController::class, 'show'], null, ['GET', 'POST'])->with('id', '[0-9]+')->with('slug', '[a-z\-0-9]+');
$router->get('/pages/:id', 'App\PageController#index');
$router->get('/posts', function() {echo 'All Posts';});
$router->get('/posts/:id', function(string $id) {echo 'Get Post ' . $id;});
$router->post('/posts/:id', function(string $id) {echo 'POSTing for Post ' . $id;});

$router = (new RouterContainer($router))->run($request);
/*$router->get('/posts/:id', function (int $id) {
?>
    <form action="" method="post">
        <input type="text" name="ok">
        <button type="submit">Send</button>
    </form>
<?php
});
$router->post('/posts/:id', function (string $id) {
    echo 'Hello' . $id . '<pre>' . print_r($_POST) . '</pre>';
})->with('id', '[0-9]+');

$router->get('/posts/:id-:slug', function (string $slug, string $id) {
    echo 'Hello ' . $slug . ':' . $id;
})->with('id', '(\d+)');

$router->get('/', fn() => 'Hello Homepage');

$router->run($request);
*/