<?php

declare(strict_types=1);

namespace Sorani\RouterGrafik\Tests;

use GuzzleHttp\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Psr\Http\Message\ServerRequestInterface;
use Sorani\RouterGrafik\Exception\NoNamedRoutesException;
use Sorani\RouterGrafik\Exception\NoRoutesFoundException;
use Sorani\RouterGrafik\NewRouter;
use Sorani\RouterGrafik\RequestContextInterface;
use Sorani\RouterGrafik\RequestContextPsr7;
use Sorani\RouterGrafik\Route;
use Sorani\RouterGrafik\Tests\Fixtures\TestController;

class NewRouterTest extends TestCase
{
    use ProphecyTrait;

    public function testRouterIsInstance()
    {
        $context = new RequestContextPsr7(new ServerRequest('GET', '/'));
        $this->assertInstanceOf(NewRouter::class, new NewRouter($context));
    }

    public function testGet()
    {
        $request = new ServerRequest('GET', '/');
        $context = new RequestContextPsr7($request);
        $router = new NewRouter($context);
        $router->get('/', fn () => 'Hello');
        $route = $router->run();
        // $this->assertInstanceOf(Route::class, $router->match());
    }


    public function testGetNoRoutWithMethod()
    {
        $request = new ServerRequest('POST', '/');
        $context = new RequestContextPsr7($request);
        $router = new NewRouter($context);
        $router->get('/', fn () => 'Hello');
        $this->expectException(\Exception::class);
        $route = $router->run();
        // $this->assertInstanceOf(Route::class, $router->match());
    }

    public function testGetWithParameter()
    {
        $request = new ServerRequest('GET', '/posts/123');
        $context = new RequestContextPsr7($request);
        $router = new NewRouter($context);
        $router->get('/posts/:id', function (string $id) {
            return 'Post ' . $id;
        });
        $route = $router->run();
        $this->assertEquals('Post 123', $route);
    }

    public function testPostWithParameter()
    {
        $request = new ServerRequest('POST', '/posts/123');
        $data = ['name' => 'Sorani'];
        $request = $request->withParsedBody($data);
        $context = new RequestContextPsr7($request);
        $router = new NewRouter($context);
        $router->post('/posts/:id', function () use ($request) {
            return 'name: ' . $request->getParsedBody()['name'];
        });
        $this->assertEquals('name: Sorani', $router->run());
    }

    public function testGenerateUri()
    {
        $context = new RequestContextPsr7($this->prophesize(ServerRequestInterface::class)->reveal());
        $router = new NewRouter($context);
        $router->add((new Route('/post/:id-:slug', function () {
        }, 'post.show'))->with('id', '[0-9]+')->with('slug', '[a-z\-0-9]+'));
        $this->assertEquals('post/123-my-post', $router->generateUri('post.show', ['id' => 123, 'slug' => 'my-post',]));
    }


    public function testAddRouteWithControllerCallable()
    {
        $context = new RequestContextPsr7($this->prophesize(ServerRequestInterface::class)->reveal());
        $router = new NewRouter($context);
        $routeName = '\Sorani\RouterGrafik\Tests\Fixtures\TestController#index';
        $router->get('/', $routeName);
        $this->assertInstanceOf(Route::class, $router->getRoute($routeName));
        $router = null;

        $context = new RequestContextPsr7($this->prophesize(ServerRequestInterface::class)->reveal());
        $router = new NewRouter($context);
        $routeName = [TestController::class, 'index'];
        $router->get('/test', $routeName);
        $this->assertInstanceOf(Route::class, $router->getRoute(implode('#', $routeName)));
        $router = null;

        $context = new RequestContextPsr7($this->prophesize(ServerRequestInterface::class)->reveal());
        $router = new NewRouter($context);
        $routeName = '\Sorani\RouterGrafik\Tests\Fixtures\TestController#index';
        $router->add(new Route('/', $routeName));
        $this->assertInstanceOf(Route::class, $router->getRoute($routeName));
        $router = null;

        $context = new RequestContextPsr7($this->prophesize(ServerRequestInterface::class)->reveal());
        $router = new NewRouter($context);
        $routeName = [TestController::class, 'index'];
        $router->add(new Route('/test', $routeName));
        $this->assertInstanceOf(Route::class, $router->getRoute(implode('#', $routeName)));
        $router = null;
    }

    public function testGenerateUriNoRouteFoundException()
    {
        $context = new RequestContextPsr7($this->prophesize(ServerRequestInterface::class)->reveal());
        $this->expectException(NoNamedRoutesException::class);
        (new NewRouter($context))->generateUri('route');
    }


    public function testGenerateUriFromControllerRoute()
    {
        $context = new RequestContextPsr7($this->prophesize(ServerRequestInterface::class)->reveal());
        $router = new NewRouter($context);
        $router->add((new Route('/post/:id-:slug', [TestController::class, 'show']))->with('id', '[0-9]+')->with('slug', '[a-z\-0-9]+'));
        $this->assertEquals('post/123-my-post', $router->generateUri(TestController::class . '#show', ['id' => 123, 'slug' => 'my-post',]));
        $router = null;


        $context = new RequestContextPsr7($this->prophesize(ServerRequestInterface::class)->reveal());
        $router = new NewRouter($context);
        $routeName = '\Sorani\RouterGrafik\Tests\Fixtures\TestController#show';
        $router->add((new Route('/post/:id-:slug', $routeName))->with('id', '[0-9]+')->with('slug', '[a-z\-0-9]+'));
        $this->assertEquals('post/123-my-post', $router->generateUri($routeName, ['id' => 123, 'slug' => 'my-post',]));
        $router = null;
    }

    public function testPostRequest()
    {
        $request = new ServerRequest('POST', '/');
        $router = new NewRouter(new RequestContextPsr7($request));
        $router->post('/', fn () => $request->getMethod());
        $route = $router->run();
        $this->assertEquals('POST', $router->run());
        // $this->assertInstanceOf(Route::class, $router->match());
    }

    public function testMap()
    {
        $request = new ServerRequest('GET', '/');
        $router = new NewRouter(new RequestContextPsr7($request));
        $router->map('/', fn () => $request->getMethod(), null, 'GET');
        $this->assertEquals('GET', $router->run());

        $request = new ServerRequest('POST', '/');
        $router = new NewRouter(new RequestContextPsr7($request));
        $router->map('/', fn () => $request->getMethod(), null, 'POST');
        $this->assertEquals('POST', $router->run());
    }

    public function testMapWithSeveralMethods()
    {

        $router = new NewRouter(new RequestContextPsr7(new ServerRequest('GET', '/')));
        $router->map('/', fn () => 'hello', null, ['GET']);

        $request = new ServerRequest('GET', '/');
        $this->assertEquals('hello', $router->run());

        new RequestContextPsr7($request);
        $router->map('/', fn () => 'hello', null, ['GET', 'POST']);

        $request = new ServerRequest('GET', '/');
        $this->assertEquals('hello', $router->run());
        $request = new ServerRequest('POST', '/');
        $this->assertEquals('hello', $router->run());
    }

    public function testNoRouteException()
    {
        $router = new NewRouter(new RequestContextPsr7(new ServerRequest('GET', '/')));
        $router->get('/ff', function () {
        });
        $this->expectException(NoRoutesFoundException::class);
        $router->run();
    }

    public function testMatchNoRouteFound()
    {
        $router = new NewRouter(new RequestContextPsr7(new ServerRequest('GET', '/')));
        $router->get('/ff', function () {
        });
        $this->assertNull($router->match());
    }

    public function testMatchRoute()
    {
        $router = new NewRouter(new RequestContextPsr7(new ServerRequest('GET', '/')));
        $router->get('/', function () {
        });
        $this->assertInstanceOf(Route::class, $router->match());
    }

    public function testGetters()
    {
        $router = new NewRouter($this->prophesize(RequestContextInterface::class)->reveal());
        $router->get('/', function () {
            return 'hello';
        }, 'test.route');
        $this->assertInstanceOf(Route::class, $router->getRoute('test.route'));
        $this->assertEquals('hello', $router->getRoute('test.route')->getCallable()());
        $this->assertEquals('test.route', $router->getRoute('test.route')->getName());

        $this->assertNull($router->getRoute('nonexistingroute'));

        $router = null;

        $router = new NewRouter($this->prophesize(RequestContextInterface::class)->reveal());
        $router->get('/', [TestController::class, 'index'], 'test.route');
        $this->assertEquals(['GET' => [new Route('/', [TestController::class, 'index'], 'test.route')]], $router->getRoutes());
        $this->assertEquals(['test.route' => new Route('/', [TestController::class, 'index'], 'test.route')], $router->getNamedRoutes());
        $router = null;
    }
}
