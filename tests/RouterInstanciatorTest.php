<?php

declare(strict_types=1);

namespace Sorani\RouterGrafik\Tests;

use GuzzleHttp\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Sorani\RouterGrafik\Route;
use Sorani\RouterGrafik\Router;
use Sorani\RouterGrafik\RouterInstanciator;
use Sorani\RouterGrafik\Tests\Fixtures\TestController;

class RouterInstanciatorTest extends TestCase
{
    public function testRouterIsInstance()
    {
        $this->assertInstanceOf(RouterInstanciator::class, new RouterInstanciator());
    }

    public function testGet()
    {
        $request = new ServerRequest('GET', '/');
        $router = new RouterInstanciator();
        $router->get('/', fn () => 'Hello');
        $route = $router->run($request);
        // $this->assertInstanceOf(Route::class, $router->match());
    }


    public function testGetNoRoutWithMethod()
    {
        $request = new ServerRequest('POST', '/');
        $router = new RouterInstanciator();
        $router->get('/', fn () => 'Hello');
        $this->expectException(\Exception::class);
        $route = $router->run($request);
        // $this->assertInstanceOf(Route::class, $router->match());
    }

    public function testGetWithParameter()
    {
        $request = new ServerRequest('GET', '/posts/123');
        $router = new RouterInstanciator();
        $router->get('/posts/:id', function (string $id) {
            return 'Post ' . $id;
        });
        $this->assertEquals('Post 123', $router->run($request));
    }

    public function testPostWithParameter()
    {
        $request = new ServerRequest('POST', '/posts/123');
        $data = ['name' => 'Sorani'];
        $request = $request->withParsedBody($data);
        $router = new RouterInstanciator();
        $router->post('/posts/:id', function () use ($request) {
            return 'name: ' . $request->getParsedBody()['name'];
        });
        $this->assertEquals('name: Sorani', $router->run($request));
    }

    public function testGenerateUri()
    {
        $router = new RouterInstanciator();
        $router->add((new Route('/post/:id-:slug', function () {
        }, 'post.show'))->with('id', '[0-9]+')->with('slug', '[a-z\-0-9]+'));
        $this->assertEquals('post/123-my-post', $router->generateUri('post.show', ['id' => 123, 'slug' => 'my-post',]));
    }


    public function testAddRouteWithControllerCallable()
    {
        $router = new RouterInstanciator();
        $routeName = '\Sorani\RouterGrafik\Tests\Fixtures\TestController#index';
        $router->get('/', $routeName);
        $this->assertInstanceOf(Route::class, $router->getRoute($routeName));
        $router = null;

        $router = new RouterInstanciator();
        $routeName = [TestController::class, 'index'];
        $router->get('/test', $routeName);
        $this->assertInstanceOf(Route::class, $router->getRoute(implode('#', $routeName)));
        $router = null;

        $router = new RouterInstanciator();
        $routeName = '\Sorani\RouterGrafik\Tests\Fixtures\TestController#index';
        $router->add(new Route('/', $routeName));
        $this->assertInstanceOf(Route::class, $router->getRoute($routeName));
        $router = null;

        $router = new RouterInstanciator();
        $routeName = [TestController::class, 'index'];
        $router->add(new Route('/test', $routeName));
        $this->assertInstanceOf(Route::class, $router->getRoute(implode('#', $routeName)));
        $router = null;
    }


    public function testGenerateUriFromControllerRoute()
    {
        $router = new RouterInstanciator();
        $router->add((new Route('/post/:id-:slug', [TestController::class, 'show']))->with('id', '[0-9]+')->with('slug', '[a-z\-0-9]+'));
        $this->assertEquals('post/123-my-post', $router->generateUri(TestController::class . '#show', ['id' => 123, 'slug' => 'my-post',]));
        $router = null;


        $router = new RouterInstanciator();
        $routeName = '\Sorani\RouterGrafik\Tests\Fixtures\TestController#show';
        $router->add((new Route('/post/:id-:slug', $routeName))->with('id', '[0-9]+')->with('slug', '[a-z\-0-9]+'));
        $this->assertEquals('post/123-my-post', $router->generateUri($routeName, ['id' => 123, 'slug' => 'my-post',]));
        $router = null;
    }

    public function testPostRequest()
    {
        $request = new ServerRequest('POST', '/');
        $router = new RouterInstanciator();
        $router->post('/', fn () => $request->getMethod());
        $route = $router->run($request);
        $this->assertEquals('POST', $router->run($request));
        // $this->assertInstanceOf(Route::class, $router->match());
    }

    public function testMap()
    {
        $request = new ServerRequest('GET', '/');
        $router = new RouterInstanciator();
        $router->map('/', fn () => $request->getMethod(), null, 'GET');
        $this->assertEquals('GET', $router->run($request));

        $request = new ServerRequest('POST', '/');
        $router = new RouterInstanciator();
        $router->map('/', fn () => $request->getMethod(), null, 'POST');
        $this->assertEquals('POST', $router->run($request));
    }

    public function testMapWithSeveralMethods()
    {

        $router = new RouterInstanciator();
        $router->map('/', fn () => 'hello', null, ['GET']);

        $request = new ServerRequest('GET', '/');
        $this->assertEquals('hello', $router->run($request));

        $router = new RouterInstanciator();
        $router->map('/', fn () => 'hello', null, ['GET', 'POST']);

        $request = new ServerRequest('GET', '/');
        $this->assertEquals('hello', $router->run($request));
        $request = new ServerRequest('POST', '/');
        $this->assertEquals('hello', $router->run($request));
    }

    public function testNoRouteFound()
    {
        $router = new RouterInstanciator();
        $router->get('/ff', function () {
        });
        $request = new ServerRequest('GET', '/test');
        $this->assertNull($router->run($request));
    }
}
