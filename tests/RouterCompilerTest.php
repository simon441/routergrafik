<?php

declare(strict_types=1);

namespace Sorani\RouterGrafik\Tests;

use GuzzleHttp\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Sorani\RouterGrafik\Exception\RouterException;
use Sorani\RouterGrafik\Route;
use Sorani\RouterGrafik\Router;
use Sorani\RouterGrafik\RouterCompiler;
use Sorani\RouterGrafik\Tests\Fixtures\TestController;

class RouterCompilerTest extends TestCase
{
    use ProphecyTrait;

    private $dir = __DIR__ . '/Fixtures/routes.php';

    public function tearDown(): void
    {
        if (file_exists($this->dir))
            unlink($this->dir);
    }

    public function testIsInstance()
    {
        $actual = new RouterCompiler($this->prophesize(Router::class)->reveal());
        $this->assertInstanceOf(RouterCompiler::class, $actual);
    }

    public function testCompileClassAsString()
    {
        $request = new ServerRequest('POST', '/posts/123');
        $data = ['name' => 'Sorani'];
        $request = $request->withParsedBody($data);
        $router = new Router();
        $router->post('/posts/:id', TestController::class);

        $routerCompiler = new RouterCompiler($router);

        $this->assertInstanceOf(RouterCompiler::class, $routerCompiler);
        $dir = $this->dir;
        $routerCompiler->compile($dir);
        $actual = require $dir;
        $this->assertEquals(
            [
                'routes' => [
                    'POST' => [
                        new \Sorani\RouterGrafik\Route('/posts/:id', \Sorani\RouterGrafik\Tests\Fixtures\TestController::class, null)
                    ]
                ],
                'namedRoutes' => [
                    \Sorani\RouterGrafik\Tests\Fixtures\TestController::class =>  new \Sorani\RouterGrafik\Route('/posts/:id', TestController::class, null)
                ]
            ],
            $actual
        );
    }

    public function testCompileClassAsArray()
    {
        $request = new ServerRequest('POST', '/posts/123');
        $data = ['name' => 'Sorani'];
        $request = $request->withParsedBody($data);
        $router = new Router();
        $router->post('/posts/:id', [TestController::class, 'index']);
        $router->post('/posts/:id', [TestController::class, 'show'], 'posts.show');

        $routerCompiler = new RouterCompiler($router);

        $this->assertInstanceOf(RouterCompiler::class, $routerCompiler);
        $dir = $this->dir;
        $routerCompiler->compile($dir);
        $actual = require $dir;
        $this->assertEquals(
            [
                'routes' => [
                    'POST' => [
                        new \Sorani\RouterGrafik\Route('/posts/:id', [\Sorani\RouterGrafik\Tests\Fixtures\TestController::class, 'index']),
                        new \Sorani\RouterGrafik\Route('/posts/:id', [\Sorani\RouterGrafik\Tests\Fixtures\TestController::class, 'show'], 'posts.show'),
                    ],
                ],
                'namedRoutes' => [
                    \Sorani\RouterGrafik\Tests\Fixtures\TestController::class . Route::CLASS_SEPARATOR . 'index' =>
                    new \Sorani\RouterGrafik\Route('/posts/:id', [\Sorani\RouterGrafik\Tests\Fixtures\TestController::class, 'index']),
                    'posts.show' =>  new \Sorani\RouterGrafik\Route('/posts/:id', [\Sorani\RouterGrafik\Tests\Fixtures\TestController::class, 'show'], 'posts.show')
                ]
            ],
            $actual
        );
    }


    public function testCompileClosure()
    {
        $request = new ServerRequest('POST', '/posts/123');
        $data = ['name' => 'Sorani'];
        $request = $request->withParsedBody($data);
        $route2 = new Route('/blog/', function () {
            return 'blog';
        });
        $router = new Router();
        $router->post('/blog/', function () {
            return 'blog';
        });
        $router->post('/blog/:id', function () {
            return 'blog';
        }, 'posts.show');

        $routerCompiler = new RouterCompiler($router);

        $this->assertInstanceOf(RouterCompiler::class, $routerCompiler);
        $dir = $this->dir;
        $routerCompiler->compile($dir);
        $actual = require $dir;
        $this->assertEquals(
            [
                'routes' => [
                    'POST' => [
                        new \Sorani\RouterGrafik\Route('/blog/', function () {
                            return 'blog';
                        }),

                        new \Sorani\RouterGrafik\Route('/blog/:id', function () {
                            return 'blog';
                        }, 'posts.show')
                    ],
                ],
                'namedRoutes' => [
                    'posts.show' => new \Sorani\RouterGrafik\Route('/blog/:id', function () {
                        return 'blog';
                    }, 'posts.show'),
                ],
            ],
            $actual
        );
    }
    public function testCompileClassAsArrayMethodNotExists()
    {
        $request = new ServerRequest('POST', '/posts/123');
        $data = ['name' => 'Sorani'];
        $request = $request->withParsedBody($data);
        $router = new Router();
        $router->post('/posts/:id', [TestController::class, 'notexists']);

        $this->expectException(RouterException::class);
        (new RouterCompiler($router))->compile($this->dir);
    }
}
