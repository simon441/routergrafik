<?php

declare(strict_types=1);

namespace Sorani\RouterGrafik\Tests;

use GuzzleHttp\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Sorani\RouterGrafik\RequestContextPsr7;

class RequestContextPsr7Test extends TestCase
{
    public function test()
    {
        $req = new ServerRequest('GET', '/');
        $request = new RequestContextPsr7($req);
        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('/', $request->getUri());

        $request = RequestContextPsr7::fromRequest($req);
        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('/', $request->getUri());
    }
}
