<?php

declare(strict_types=1);

namespace Sorani\RouterGrafik\Tests\Fixtures;

use Psr\Http\Message\ServerRequestInterface;

class TestController
{
    private $request;
    public function __construct(ServerRequestInterface $request)
    {
        $this->request = $request;
    }
    public function index(int $id)
    {
        return 'Post ' . $id;
    }

    public function show(int $id, string $slug)
    {
        // if ($this->request->getMethod() === 'POST')
        return sprintf('Post %d - slug: %s', $id, $slug);
    }

}
