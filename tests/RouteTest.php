<?php

declare(strict_types=1);

namespace Sorani\RouterGrafik\Tests;

use GuzzleHttp\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Psr\Http\Message\ServerRequestInterface;
use Sorani\RouterGrafik\Route;
use Sorani\RouterGrafik\Tests\Fixtures\TestController;

class RouteTest extends TestCase
{
    use ProphecyTrait;

    public function testRouterIsInstance()
    {
        $this->assertInstanceOf(Route::class, new Route('/', null));
    }

    public function testGetCallableSimplePath()
    {
        $fn = function () {
            return 'Hello';
        };
        $route = new Route('/', $fn);
        $route2 = new Route('/blog/', function () {
            return 'blog';
        });
        $this->assertTrue($route->match('/'));
        $this->assertTrue($route2->match('/blog'));
        $this->assertEquals('Hello', $route->getCallable()());
        $this->assertEquals('Hello', $route->execute());
        $this->assertEquals('blog', $route2->getCallable()());
        $this->assertEquals('blog', $route2->execute());
    }

    public function testMatchSimplePath()
    {
        $fn = function () {
            return 'Hello';
        };
        $route = new Route('/', $fn);
        $route2 = new Route('/blog', function () {
            return 'blog';
        });
        $this->assertTrue($route->match('/'));
        $this->assertTrue($route->match('/blog'));
    }

    public function testGetWithParameters()
    {
        $fn = function (string $id) {
            return 'Post ' . $id;
        };
        $route = new Route('/post/:id', $fn);
        $this->assertTrue($route->match('/post/123'));
        $this->assertEquals('Post 123', $route->execute());
    }

    public function testPostWithParameters()
    {
        $fn = function (string $id) {
            return 'Post ' . $id;
        };
        $route = new Route('/post/:id', $fn);
        $this->assertTrue($route->match('/post/123'));
        $this->assertEquals('Post 123', $route->execute());
    }

    public function testGetWithSeveralParameters()
    {
        $fn = function (string $id, string $slug) {
            return 'Post ' . $id . '- slug: ' . $slug;
        };
        $route = (new Route('/post/:id-:slug', $fn))->with('id', '[0-9]+')->with('slug', '[a-z\-0-9]+');
        $this->assertTrue($route->match('/post/123-my-post'));
        $this->assertEquals('Post 123- slug: my-post', $route->execute());
    }


    public function testWithComplexPattern()
    {
        $fn = function (string $id, string $slug) {
            return 'Post ' . $id . '- slug: ' . $slug;
        };
        $route = (new Route('/post/:id-:slug', $fn))->with('id', '[0-9]+')->with('slug', '([a-z\-0-9]+)');
        $this->assertTrue($route->match('/post/123-my-post'));
        $this->assertEquals(2, count($route->getMatches()));
        $this->assertEquals('Post 123- slug: my-post', $route->execute());
    }


    public function testGenerateUri()
    {
        $route = (new Route('/post/:id-:slug', function () {
        }, 'post.show'));
        $this->assertEquals('post/123-my-post', $route->generateUri(['id' => 123, 'slug' => 'my-post',]));
    }


    public function testController()
    {

        $request = $this->prophesize(ServerRequestInterface::class);
        $route = new Route('/post/:id', [TestController::class, 'index']);
        $this->assertTrue($route->match('/post/123'));
        $this->assertEquals('Post 123', $route->execute($request->reveal()));
        $route = new Route('/post/:id', '\Sorani\RouterGrafik\Tests\Fixtures\TestController#index');
        $this->assertTrue($route->match('/post/123'));
        $this->assertEquals('Post 123', $route->execute($request->reveal()));
        
        $route = (new Route('/post/:id-:slug', '\Sorani\RouterGrafik\Tests\Fixtures\TestController#show'))->with('id', '[0-9]+')->with('slug', '[a-z\-0-9]+');
        $this->assertTrue($route->match('/post/123-my-post'));
        $this->assertEquals('Post 123 - slug: my-post', $route->execute($request->reveal()));
    }

    /**
     * @requirde OS linux
     */
    public function testGetWithSeveralParametersNotSameOrder()
    {
        $fn = function (string $slug, string $id) {
            return 'Post ' . $id . '- slug: ' . $slug;
        };
        $route = (new Route('/post/:id-:slug', $fn))->with('id', '[0-9]+')->with('slug', '[a-z\-0-9]+');
        $this->assertTrue($route->match('/post/123-my-post'));

        $this->assertEquals('Post 123- slug: my-post', $route->execute());
        $request = $this->prophesize(ServerRequestInterface::class);
        $route = (new Route('/post/:slug-:id', '\Sorani\RouterGrafik\Tests\Fixtures\TestController#show'))->with('id', '[0-9]+')->with('slug', '[a-z\-0-9]+');
        $this->assertTrue($route->match('/post/my-post-123'));
        $this->assertEquals('Post 123 - slug: my-post', $route->execute($request->reveal()));
    }
}
