<?php

declare(strict_types=1);

namespace Sorani\RouterGrafik\Tests;

use GuzzleHttp\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Sorani\RouterGrafik\Exception\NoRoutesFoundException;
use Sorani\RouterGrafik\Route;
use Sorani\RouterGrafik\Router;
use Sorani\RouterGrafik\Tests\Fixtures\TestController;

class RouterTest extends TestCase
{
    public function testRouterIsInstance()
    {
        $this->assertInstanceOf(Router::class, new Router());
    }

    public function testGet()
    {
        $request = new ServerRequest('GET', '/');
        $router = new Router();
        $router->get('/', fn () => 'Hello');
        $route = $router->run($request);
        // $this->assertInstanceOf(Route::class, $router->match());
    }


    public function testGetNoRoutWithMethod()
    {
        $request = new ServerRequest('POST', '/');
        $router = new Router();
        $router->get('/', fn () => 'Hello');
        $this->expectException(\Exception::class);
        $route = $router->run($request);
        // $this->assertInstanceOf(Route::class, $router->match());
    }

    public function testGetWithParameter()
    {
        $request = new ServerRequest('GET', '/posts/123');
        $router = new Router();
        $router->get('/posts/:id', function (string $id) {
            return 'Post ' . $id;
        });
        $this->assertEquals('Post 123', $router->run($request)->execute());
    }

    public function testPostWithParameter()
    {
        $request = new ServerRequest('POST', '/posts/123');
        $data = ['name' => 'Sorani'];
        $request = $request->withParsedBody($data);
        $router = new Router();
        $router->post('/posts/:id', function () use ($request) {
            return 'name: ' . $request->getParsedBody()['name'];
        });
        $this->assertEquals('name: Sorani', $router->run($request)->execute());
    }

    public function testGenerateUri()
    {
        $router = new Router();
        $router->add((new Route('/post/:id-:slug', function () {
        }, 'post.show'))->with('id', '[0-9]+')->with('slug', '[a-z\-0-9]+'));
        $this->assertEquals('post/123-my-post', $router->generateUri('post.show', ['id' => 123, 'slug' => 'my-post',]));
    }


    public function testAddRouteWithControllerCallable()
    {
        $router = new Router();
        $routeName = '\Sorani\RouterGrafik\Tests\Fixtures\TestController#index';
        $router->get('/', $routeName);
        $this->assertInstanceOf(Route::class, $router->getRoute($routeName));
        $router = null;

        $router = new Router();
        $routeName = [TestController::class, 'index'];
        $router->get('/test', $routeName);
        $this->assertInstanceOf(Route::class, $router->getRoute(implode('#', $routeName)));
        $router = null;

        $router = new Router();
        $routeName = '\Sorani\RouterGrafik\Tests\Fixtures\TestController#index';
        $router->add(new Route('/', $routeName));
        $this->assertInstanceOf(Route::class, $router->getRoute($routeName));
        $router = null;

        $router = new Router();
        $routeName = [TestController::class, 'index'];
        $router->add(new Route('/test', $routeName));
        $this->assertInstanceOf(Route::class, $router->getRoute(implode('#', $routeName)));
        $router = null;
    }


    public function testGenerateUriFromControllerRoute()
    {
        $router = new Router();
        $router->add((new Route('/post/:id-:slug', [TestController::class, 'show']))->with('id', '[0-9]+')->with('slug', '[a-z\-0-9]+'));
        $this->assertEquals('post/123-my-post', $router->generateUri(TestController::class . '#show', ['id' => 123, 'slug' => 'my-post',]));
        $router = null;


        $router = new Router();
        $routeName = '\Sorani\RouterGrafik\Tests\Fixtures\TestController#show';
        $router->add((new Route('/post/:id-:slug', $routeName))->with('id', '[0-9]+')->with('slug', '[a-z\-0-9]+'));
        $this->assertEquals('post/123-my-post', $router->generateUri($routeName, ['id' => 123, 'slug' => 'my-post',]));
        $router = null;
    }

    public function testPostRequest()
    {
        $request = new ServerRequest('POST', '/');
        $router = new Router();
        $router->post('/', fn () => $request->getMethod());
        $route = $router->run($request);
        $this->assertEquals('POST', $router->run($request)->execute());
        // $this->assertInstanceOf(Route::class, $router->match());
    }

    public function testMap()
    {
        $request = new ServerRequest('GET', '/');
        $router = new Router();
        $router->map('/', fn () => $request->getMethod(), null, 'GET');
        $this->assertEquals('GET', $router->run($request)->execute());

        $request = new ServerRequest('POST', '/');
        $router = new Router();
        $router->map('/', fn () => $request->getMethod(), null, 'POST');
        $this->assertEquals('POST', $router->run($request)->execute());
    }

    public function testMapWithSeveralMethods()
    {

        $router = new Router();
        $router->map('/', fn () => 'hello', null, ['GET']);

        $request = new ServerRequest('GET', '/');
        $this->assertEquals('hello', $router->run($request)->execute());

        $router = new Router();
        $router->map('/', fn () => 'hello', null, ['GET', 'POST']);

        $request = new ServerRequest('GET', '/');
        $this->assertEquals('hello', $router->run($request)->execute());
        $request = new ServerRequest('POST', '/');
        $this->assertEquals('hello', $router->run($request)->execute());
    }

    public function testNoRouteException()
    {
        $router = new Router();
        $router->get('/ff', function () {
        });
        $this->expectException(NoRoutesFoundException::class);
        $request = new ServerRequest('GET', '/test');
        $router->run($request);
    }
}
