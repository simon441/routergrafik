<?php

declare(strict_types=1);

namespace Sorani\RouterGrafik\Tests;

use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Sorani\RouterGrafik\Utils;

class UtilsTest extends TestCase
{
    public function testGetClosureAsStringNoArguments()
    {
        $closure = function () {
            echo 'b';
        };

        $this->assertEquals('function () {
            echo \'b\';
        };' . "\n", Utils::getClosureAsString($closure));
    }

    public function testGetClosureAsStringWithArguments()
    {
        $closure = function ($a, $b) {
            echo $a . ' ' . $b;
        };

        $this->assertEquals('function ($a, $b) {
            echo $a . \' \' . $b;
        };' . "\n", Utils::getClosureAsString($closure));
    }

    public function testGetClosureAsStringWithTypedArguments()
    {
        $closure = function (int $a, string $b) {
            echo $a . ' ' . $b;
        };

        $this->assertEquals('function (int $a, string $b) {
            echo $a . \' \' . $b;
        };' . "\n", Utils::getClosureAsString($closure));


        $closure = function (array $a, \Serializable $b) {
            echo $a . ' ' . $b;
        };

        $this->assertEquals('function (array $a, \Serializable $b) {
            echo $a . \' \' . $b;
        };' . "\n", Utils::getClosureAsString($closure));
    }


    public function testGetClosureAsStringWithReferencedArguments()
    {
        $closure = function (int $a, string &$b) {
            echo $a . ' ' . $b;
        };

        $this->assertEquals('function (int $a, string &$b) {
            echo $a . \' \' . $b;
        };' . "\n", Utils::getClosureAsString($closure));


        $c = '';
        $closure = function (array $a, string &$b) use (&$c) {
            echo $a . ' ' . $b;
        };

        $this->assertEquals('function (array $a, string &$b) use (&$c) {
            echo $a . \' \' . $b;
        };' . "\n", Utils::getClosureAsString($closure));
    }


    public function testGetClosureAsStringWithDefaultArguments()
    {
        $closure = function (int $a, string $b, string $p = 'a', array $options = [], ?\JsonSerializable $jsonSerializable = null) {
            echo $a . ' ' . $b;
        };

        $this->assertEquals('function (int $a, string $b, string $p = \'a\', array $options = array(), ?\JsonSerializable $jsonSerializable = null) {
            echo $a . \' \' . $b;
        };' . "\n", Utils::getClosureAsString($closure));
    }

    public function testGetClosureAsStringWithNew()
    {
        $k = '';
        $closure = function (int $a, string $b) use ($k) {
            return new \stdClass();
        };

        $this->assertEquals('function (int $a, string $b) use ($k) {
            return new \stdClass();
        };' . "\n", Utils::getClosureAsString($closure));


        $k = '';
        $closure = function (ContainerInterface $c, string $b) use ($k) {
            $std = new \stdClass();
            return new \Sorani\DependancyInjection\Container\Tests\Fixtures\SingletonClass();
        };

        $this->assertEquals('function (\Psr\Container\ContainerInterface $c, string $b) use ($k) {
            $std = new \stdClass();
            return new \Sorani\DependancyInjection\Container\Tests\Fixtures\SingletonClass();
        };' . "\n", Utils::getClosureAsString($closure));
    }


    public function testGetClosureAsStringFialsWithNotExistingClass()
    {
        $k = '';
        $closure = function (int $a, string $b) use ($k) {
            return new NotExistsClass();
        };

        $this->expectException(\ReflectionException::class);
        Utils::getClosureAsString($closure);
    }

    public function testGetClosureAsStringWithTypedArgumentsAndUse()
    {
        $k = '';
        $closure = function (int $a, string $b) use ($k) {
            echo $a . ' ' . $b;
        };

        $this->assertEquals('function (int $a, string $b) use ($k) {
            echo $a . \' \' . $b;
        };' . "\n", Utils::getClosureAsString($closure));


        $k = '';
        $closure = function (ContainerInterface $c, string $b) use ($k) {
            return new \stdClass();
        };

        $this->assertEquals('function (\Psr\Container\ContainerInterface $c, string $b) use ($k) {
            return new \stdClass();
        };' . "\n", Utils::getClosureAsString($closure));
    }
}
