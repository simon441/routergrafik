<?php

declare(strict_types=1);

namespace Sorani\RouterGrafik\Tests;

use GuzzleHttp\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Sorani\RouterGrafik\RequestContextPhp;
use Sorani\RouterGrafik\RequestContextPsr7;

class RequestContextPhpTest extends TestCase
{
    public function test()
    {
        $_SERVER['REQUEST_METHOD'] = 'GET';
        $request = new RequestContextPhp('/');
        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('/', $request->getUri());

        $request = RequestContextPhp::fromRequest('/');
        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('/', $request->getUri());
    }
}
