<?php

declare(strict_types=1);

namespace Sorani\RouterGrafik;

use Psr\Http\Message\ServerRequestInterface;

class Route
{

    public const CLASS_SEPARATOR = '#';

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $path;

    /**
     * @var callable|array|string
     */
    private $callable;

    /**
     * @var string[] list of matches found
     */
    private $matches;

    /**
     * @var string[] list of contraints on the parameters
     */
    private $parametersConstraints = [];

    /**
     * Route Constructor
     *
     * @param  string $path
     * @param  mixed $callable
     * @param  string|null $name
     */
    public function __construct(string $path, $callable, ?string $name = null)
    {
        $this->path = trim($path, '/');
        $this->callable = $callable;
        $this->name = $name;
    }

    /**
     * Get the value of callable
     *
     * @return  callable|array|string
     */
    public function getCallable()
    {
        return $this->callable;
    }


    /**
     * Add a constraint to a parameter as a Regular Expression
     * 
     * @param  string $parameter
     * @param  string $pattern
     * @return self
     */
    public function with(string $parameter, string $pattern): self
    {
        $this->parametersConstraints[$parameter] = str_replace('(', '(?:', $pattern);
        return $this;
    }

    /**
     * match the registered path  to a URI
     *
     * @param  string $url
     * @return bool
     */
    public function match(string $url): bool
    {
        $url = trim($url, '/');
        $path = preg_replace_callback('/:(\w+)/', function ($match) {
            if (isset($this->parametersConstraints[$match[1]])) {
                return sprintf('(%s)', $this->parametersConstraints[$match[1]]);
            }
            return '([^/]+)';
        }, $this->path);
        $pattern = sprintf('#%s#i', $path);

        if (!preg_match($pattern, $url, $matches)) {
            return false;
        }

        array_shift($matches);
        $args = [];
        preg_match_all('/\:(\w+)/u', $this->path, $matchedParameters);
        array_shift($matchedParameters);

        if (count($matchedParameters) > 0 && isset($matchedParameters[0]) && count($matchedParameters[0]) === count($matches)) {
            $parameters = array_combine($matchedParameters[0], $matches);
            $callable = $this->callable;

            if (is_string($callable)) {
                $callable = explode(self::CLASS_SEPARATOR, $callable);
            }

            if (is_array($callable)) {
                $reflection = (new \ReflectionClass($callable[0]))->getMethod($callable[1]);
            } else {
                $reflection = new \ReflectionFunction($callable);
            }
            if ($reflection !== null) {
                $args = array_map(function (\ReflectionParameter $p) {
                    return $p->getName();
                }, $reflection->getParameters());
                // var_dump($args, $matches, $path, $this->path, $m);
                // die;
                $matches = array_map(function (string $name) use ($parameters) {
                    return $parameters[$name];
                }, $args);
            }
        }
        $this->matches = $matches;

        return true;
    }

    public function execute(ServerRequestInterface $request = null)
    {
        if (is_string($this->callable)) {
            $callable = explode(self::CLASS_SEPARATOR, $this->callable);
            return $this->instanciateClass($callable[0], $callable[1], $request);
        } elseif (is_array($this->callable)) {
            return $this->instanciateClass($this->callable[0], $this->callable[1], $request);
        }
        return call_user_func_array($this->callable, $this->matches);
    }

    /**
     * Get list of matches found
     *
     * @return  string[]
     */
    public function getMatches()
    {
        return $this->matches;
    }

    /**
     * Get the value of name
     *
     * @return  string
     */
    public function getName()
    {
        return $this->name;
    }

    public function generateUri(array $parameters = []): string
    {
        $path = $this->path;
        foreach ($parameters as $k => $v) {
            $path = str_replace(":$k", $v, $path);
        }
        return $path;
    }


    /**
     * Intantiate a class and calls its method
     *
     * @param  string $object
     * @param string $method
     * @return mixed
     */
    private function instanciateClass(string $object, string $method, ServerRequestInterface $request)
    {
        $class = new $object($request);
        return call_user_func_array([$class, $method], $this->matches);
    }

    /**
     * Get the value of path
     *
     * @return  string
     */ 
    public function getPath()
    {
        return $this->path;
    }
}
