<?php
declare(strict_types=1);

namespace Sorani\RouterGrafik;

use Psr\Http\Message\ServerRequestInterface;

class RequestContextPsr7 implements RequestContextInterface
{

    /**
     * @var ServerRequestInterface
     */
    private $request;

    /**
     * @param ServerRequestInterface $request
     */
    public function __construct(ServerRequestInterface $request)
    {
        $this->request = $request;
    }

    /**
     * Add needed parameters in implementing classes to populate the variables
     * @param ServerRequestInterface
     */
    public static function fromRequest($context)
    {
        return new self($context);
    }

    /**
     * Get the Request URI
     * 
     * @return string
     */
    public function getUri(): string
    {
        return $this->request->getUri()->getPath();
    }

    /**
     * Get the Server REQUEST_METHOD
     * 
     * @return string
     */
    public function getMethod():string
    {
        return $this->request->getMethod();
    }
}