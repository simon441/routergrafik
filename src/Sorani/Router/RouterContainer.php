<?php

declare(strict_types=1);

namespace Sorani\RouterGrafik;

use Psr\Http\Message\ServerRequestInterface;

class RouterContainer
{ 
    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function run(ServerRequestInterface $request)
    {
        $route = $this->router->run($request);

        if (null !== $route) {
            return $route->execute($request);
        }
        return null;
    }

    /**
     * Get the value of router
     *
     * @return  RouterInterface
     */ 
    public function getRouter()
    {
        return $this->router;
    }
}
