<?php

declare(strict_types=1);

namespace Sorani\RouterGrafik;

use Psr\Http\Message\ServerRequestInterface;
use Sorani\RouterGrafik\Exception\NoNamedRoutesException;
use Sorani\RouterGrafik\Exception\NoRoutesFoundException;

interface RouterInterface
{
    /**
     * GET method
     *
     * @param  string $path 
     * @param  mixed $callable
     * @param  string $name Route name
     * @return Route
     */
    public function get(string $path, $callable, ?string $name = null): Route;

    /**
     * POST method
     *
     * @param  string $path
     * @param  mixed $callable
     * @param  string $name Route name
     * @return Route
     */
    public function post(string $path, $callable, ?string $name = null): Route;


    /**
     * Maps a Route with a given HTTP method
     *
     * @param  string $path
     * @param  mixed  $callable
     * @param  string $name Route name
     * @param  string|array $method HTTP METHOD (GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD), default is GET
     * @return Route
     */
    public function map(string $path, $callable, ?string $name = null, $method = 'GET');

    /**
     * Add a Route object
     *
     * @param  Route $route
     * @return self
     */
    public function add(Route $route): self;

    /**
     * run the Router
     *
     * @param  ServerRequestInterface $request
     * @return mixed
     * @throws NoRoutesFoundException
     */
    public function run(ServerRequestInterface $request);

    /**
     * Generate an URI
     *
     * @param  string $name Route name
     * @param  array $parameters
     * @return string
     * @throws NoNamedRoutesException
     */
    public function generateUri(string $name, array $parameters = []): string;

    /**
     * Get a Route by its name
     *
     * @param  string $name
     * @return Route
     */
    public function getRoute(string $name): ?Route;

    /**
     * Get the value of routes
     *
     * @return  Route[][]
     */
    public function getRoutes();

    /**
     * Get routes indexed by their name
     *
     * @return  Route[]
     */
    public function getNamedRoutes();
}
