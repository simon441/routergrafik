<?php

declare(strict_types=1);

namespace Sorani\RouterGrafik;

use Sorani\RouterGrafik\Exception\RouterException;

class RouterCompiler
{

    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }
    public function compile(string $pathToCompiledDir): void
    {
        $data = '';

        $routes = $this->router->getRoutes();

        if (!empty($routes)) {
            $data = "'routes' => [\n";
            foreach ($routes as $method => $r) {
                $data .= "'$method' => [\n";
                $data .= $this->makeRoutes($r);
            }
        }
        $namedRoutes = $this->router->getNamedRoutes();
        if (!empty($namedRoutes)) {
            $data .= "\n]\n, 'namedRoutes' => [\n";
            $data .= $this->makeRoutes($namedRoutes, true);
        }

        $data = "<?php\nreturn [" . $data . "\n];\n";
        file_put_contents($pathToCompiledDir, $data);
    }

    public function makeRoutes(array $routes, $named = false)
    {
        $data = '';

        foreach ($routes as $key => $route) {
            $path = $route->getPath();
            $name = $route->getName();
            $callable = $route->getCallable();
            $isClosure = false;
            if (is_string($callable) && class_exists($callable)) {
                $callable = '"' . (new \ReflectionClass($callable))->getName() . '"';
            } elseif (is_array($callable) && class_exists($callable[0])) {
                if (!method_exists($callable[0], $callable[1])) {
                    throw new RouterException("Cannot compile route " . $callable[0] . 'because method ' . $callable[1] . ' does not exists in the class');
                }
                $callable = '["' . (new \ReflectionClass($callable[0]))->getName() . '", "' . $callable[1] . '"]' . "\n";
            } else            if ($callable instanceof \Closure) {
                $callable = Utils::getClosureAsString($callable);
                $isClosure = true;
            }
            $d = sprintf('%s new \Sorani\RouterGrafik\Route("%s", %s, %s),', $named ? '"' . $key . '" =>' : '', $path, $callable, $name ? '"' .
                $name . '"' : 'null');
            if ($isClosure) {
                if ($name) {
                    $d = preg_replace('/,\s*' . ($name ? '("|\')' .
                        preg_quote($name, '/') . '("|\')' .
                        '\s{0,}\)\s{0,};' : 'null\),') . '/ui', '', $d);
                } else {
                    $d = rtrim(rtrim(preg_replace('/,\snull\),$/', '', $d)), ';') . ',';
                }
            }
            $data .= $d;
        }
        $data .= '],';
        return $data;
    }
}
