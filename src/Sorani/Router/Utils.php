<?php

declare(strict_types=1);

namespace Sorani\RouterGrafik;

class Utils
{
    /**
     * Get the contents of a closure as a string
     *
     * @param  \Closure $c
     * @return string
     */
    public static function getClosureAsString(\Closure $c): string
    {
        $r = new \ReflectionFunction($c);
        $param = '';
        foreach ($r->getParameters() as $p) {
            $s = '';
            if ($p->allowsNull() && $p->isDefaultValueAvailable()) {
                $s .= '?';
            }
            if ($p->isArray()) {
                $s .= 'array ';
            } elseif ($p->getClass()) {
                $s .= '\\' . $p->getClass()->name . ' ';
            } elseif ($p->hasType()) {
                $s .= $p->getType()->getName() . ' ';
            }
            if ($p->isPassedByReference()) {
                $s .= '&';
            }
            $s .= '$' . $p->getName();
            if ($p->isOptional()) {
                $s .= ' = ' . str_replace(["\n", 'array (', 'NULL'], ["", "array(", 'null'], var_export($p->getDefaultValue(), true));
            }
            // if ($p->isDefaultValueAvailable()) {
            //     $s .= ' = ' . var_export($p->getDefaultValue(), true);
            // }
            // var_dump($p, $p->isOptional(), $p->isDefaultValueAvailable());
            $param .= $s . ', ';
        }
        $param = substr($param, 0, -2);


        $body = '';
        $lines = file($r->getFileName());
        for ($l = $r->getStartLine(); $l < $r->getEndLine(); $l++) {
            $body .= $lines[$l];
        }

        // use
        $use = [];
        foreach ($r->getStaticVariables() as $static => $value) {
            if (false === strpos($body, '$' . $static)) {
                $use[] = ($p->isPassedByReference() ? '&' : '') . '$' . $static;
            }
        }
        // pack all
        $param = 'function (' . $param . ')' . (!empty($use) ? sprintf(' use (%s)', implode(', ', $use)) : '') . ' {' . "\n" . $body;

        // replace new class with FQN
        if (preg_match('/new\s{1,}(?<cls>.+)\((?<args>.*)\);/', $param, $m)) {
            $reflection = new \ReflectionClass($m['cls']);

            $param = str_replace('/new\s{1,}(?<cls>.+)\(', 'new ' . $reflection->getName(), $param);
        }
        return $param;
    }
}
