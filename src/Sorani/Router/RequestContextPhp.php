<?php

declare(strict_types=1);

namespace Sorani\RouterGrafik;

use Psr\Http\Message\ServerRequestInterface;

class RequestContextPhp implements RequestContextInterface
{

    /**
     * @var string
     */
    private $uri;

    /**
     * @param string $request
     */
    public function __construct($uri)
    {
        $this->uri = $uri;
    }

    /**
     * Add needed parameters in implementing classes to populate the variables
     * context : uri
     * @param string
     */
    public static function fromRequest($context)
    {
        return new self($context);
    }

    /**
     * Get the Request URI
     * 
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * Get the Server REQUEST_METHOD
     * 
     * @return string
     */
    public function getMethod(): string
    {
        return $_SERVER['REQUEST_METHOD'];
    }
}
