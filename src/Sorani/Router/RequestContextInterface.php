<?php
declare(strict_types=1);

namespace Sorani\RouterGrafik;

interface RequestContextInterface
{

    /**
     * Add needded parameters in implementing classes to populate the variables
     */
    public static function fromRequest($context);

    /**
     * Get the Request URI
     * 
     * @return string
     */
    public function getUri(): string;

    /**
     * Get the Server REQUEST_METHOD
     * 
     * @return string
     */
    public function getMethod(): string;
}