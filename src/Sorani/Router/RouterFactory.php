<?php
declare(strict_types=1);

namespace Sorani\RouterGrafik;

class RouterFactory
{
    public function createRouter(): RouterContainer
    {
        $router = new Router();
        $router = new RouterContainer($router);
        return $router;
    }
}