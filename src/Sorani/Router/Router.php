<?php

declare(strict_types=1);

namespace Sorani\RouterGrafik;

use Psr\Http\Message\ServerRequestInterface;
use Sorani\RouterGrafik\Exception\NoRoutesFoundException;
use Sorani\RouterGrafik\Exception\RequestMethodNotExistsException;

class Router extends AbstractRouter
{
    /**
     * run the Router
     *
     * @param ServerRequestInterface $request
     * @return mixed
     * @throws NoRoutesFoundException
     */
    public function run(ServerRequestInterface $request)
    {
        $uri = $request->getUri()->getPath();
        $routes = $this->getRoutes();
        if (!isset($routes[$request->getMethod()])) {
            throw new RequestMethodNotExistsException();
        }
        /** @var Route $route */
        foreach ($routes[$request->getMethod()] as $route) {
            if ($route->match($uri)) {
                return $route;
            }
        }
        throw new NoRoutesFoundException();
    }
}
