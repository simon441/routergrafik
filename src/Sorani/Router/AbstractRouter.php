<?php

declare(strict_types=1);

namespace Sorani\RouterGrafik;

use Psr\Http\Message\ServerRequestInterface;
use Sorani\RouterGrafik\Exception\NoNamedRoutesException;
use Sorani\RouterGrafik\Exception\NoRoutesFoundException;
use Sorani\RouterGrafik\Exception\RequestMethodNotExistsException;

abstract class AbstractRouter implements RouterInterface
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var Route[][]
     */
    private $routes = [];

    /**
     * @var Route[] Routes indexed by their name
     */
    private $namedRoutes = [];

    /**
     * Router Constructor
     *
     * @param  string|null $url
     */
    public function __construct(?string $url = null)
    {
        $this->url = $url;
    }

    /**
     * GET method
     *
     * @param  string $path 
     * @param  mixed $callable
     * @param  string $name Route name
     * @return Route
     */
    public function get(string $path, $callable, ?string $name = null): Route
    {
        return $this->map($path, $callable, $name, 'GET');
    }

    /**
     * POST method
     *
     * @param  string $path
     * @param  mixed $callable
     * @param  string $name Route name
     * @return Route
     */
    public function post(string $path, $callable, ?string $name = null): Route
    {
        return $this->map($path, $callable, $name, 'POST');
    }


    /**
     * Maps a Route with a given HTTP method
     *
     * @param  string $path
     * @param  mixed  $callable
     * @param  string $name Route name
     * @param  string|array $method HTTP METHOD (GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD), default is GET
     * @return Route
     */
    public function map(string $path, $callable, ?string $name = null, $method = 'GET'): Route
    {
        $route = new Route($path, $callable, $name);

        if (is_array($method)) {
            foreach ($method as $m) {
                $this->routes[$m][] = $route;
            }
        } else {
            $this->routes[$method][] = $route;
        }

        if (null === $name) {
            if (is_array($callable)) {
                $name = implode(Route::CLASS_SEPARATOR, $callable);
            } elseif (is_string($callable)) {
                $name = $callable;
            }
        }

        if (null !== $name) {
            $this->namedRoutes[$name] = $route;
        }

        return $route;
    }

    /**
     * Add a Route object
     *
     * @param  Route $route
     * @return self
     */
    public function add(Route $route): self
    {
        $this->routes[] = $route;
        $name = $route->getName();
        if ($name === null) {
            if (is_array($route->getCallable())) {
                $name = implode(Route::CLASS_SEPARATOR, $route->getCallable());
            } elseif (is_string($route->getCallable())) {
                $name = $route->getCallable();
            }
        }
        if (null !== $name) {
            $this->namedRoutes[$name] = $route;
        }

        return $this;
    }





    /**
     * run the Router
     *
     * @param  ServerRequestInterface $request
     * @return mixed
     * @throws NoRoutesFoundException
     */
    abstract public function run(ServerRequestInterface $request);

    /**
     * Generate an URI
     *
     * @param  string $name Route name
     * @param  array $parameters
     * @return string
     * @throws NoNamedRoutesException
     */
    public function generateUri(string $name, array $parameters = []): string
    {
        if (!isset($this->namedRoutes[$name])) {
            throw new NoNamedRoutesException();
        }
        return $this->namedRoutes[$name]->generateUri($parameters);
    }

    /**
     * Get a Route by its name
     *
     * @param  string $name
     * @return Route
     */
    public function getRoute(string $name): ?Route
    {
        if (isset($this->namedRoutes[$name])) {
            return $this->namedRoutes[$name];
        }
        return null;
    }

    /**
     * Get the value of routes
     *
     * @return  Route[][]
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * Get routes indexed by their name
     *
     * @return  Route[]
     */
    public function getNamedRoutes()
    {
        return $this->namedRoutes;
    }
}
