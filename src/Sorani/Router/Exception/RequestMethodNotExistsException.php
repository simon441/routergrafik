<?php
declare(strict_types=1);
namespace Sorani\RouterGrafik\Exception;

class RequestMethodNotExistsException extends \Exception
{}