<?php

declare(strict_types=1);

namespace Sorani\RouterGrafik;

use Psr\Http\Message\ServerRequestInterface;
use Sorani\RouterGrafik\Exception\NoRoutesFoundException;

class RouterInstanciator extends Router
{
    /**
     * {@inheritdoc}
     */
    public function run(ServerRequestInterface $request)
    {
        try {
            $route = parent::run($request);
            if (null !== $route) {
                return $route->execute($request);
            }
        } catch (NoRoutesFoundException $e) {
        }
        return null;
    }
}
